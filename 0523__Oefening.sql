USE ModernWays;

SET SQL_SAFE_UPDATES = 0;
UPDATE huisdieren
SET leeftijd = 9
WHERE Baasje ='Christiane' AND soort ='hond'
OR 	  Baasje = 'Bert' AND Soort = 'kat';
Set SQL_SAFE_UPDATES = 1;